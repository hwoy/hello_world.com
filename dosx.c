#include "dosx.h"

struct REG int0x21(const struct REG* reg)
{
	struct REG oreg;
	__asm__ volatile("int $0x21":
	"=a" (oreg.eax.value), "=b" (oreg.ebx.value), "=c" (oreg.ecx.value), "=d" (oreg.edx.value):
	"a" (reg->eax.value), "b" (reg->ebx.value), "c" (reg->ecx.value), "d" (reg->edx.value)); 
			
	return oreg;

}
