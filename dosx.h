#ifndef __DOSX_H__
#define __DOSX_H__

#include <stdint.h>

union REG16
{
	struct
	{
		uint16_t L:8;
		uint16_t H:8;
	}bit;
	uint16_t value;
};

union REG32
{
	union REG16 reg16;
	uint32_t value;

};

struct REG
{
	union REG32 eax,ebx,ecx,edx;
};



struct REG int0x21(const struct REG* reg);




#endif
