#include "dosx.h"
#include "stdio.h"

int putchar(int ch)
{
	struct REG reg;
	reg.edx.value=ch;
	reg.eax.reg16.bit.H=0x2;

	int0x21(&reg);

	return 0;
}

int puts(const char *str)
{
	while(*str) putchar(*str++);

	putchar(13); putchar(10);
	return 0;
}
