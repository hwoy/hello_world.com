CC=clang
LD=ld.lld
CFLAGS=-target i486-pc-linux-gnu -m16
CFLAGS+=-O2 -std=c99 -pedantic -Wall -ffreestanding
BIN=hw.com

.PHONY: all clean

all: $(BIN)

$(BIN): crt.o dosx.o main.o libc.o
	$(LD) -N -e _start --oformat binary -Ttext 0x100 -Map=hell.map -o $(BIN) crt.o dosx.o main.o libc.o

clean:
	del *.o *.com *.map


crt.o: crt.c dosx.h
dosx.o: dosx.c dosx.h
main.o: main.c stdio.h

libc.o: libc.c dosx.h stdio.h
	$(CC) -flto -o libc.o -c $(CFLAGS) libc.c